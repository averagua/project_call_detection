
"""
Created on Thu Jun 30 05:57:59 2016

@author: 1085la
"""
# Extract features from the file in 'parameters.py'
# save them in 'Features.csv'
'''
Ejemplo de ejecucion:

#Crear la clase, indicar la serie de tiempo y los parametros
Extractor = FeaturesExtraction(snd_mono=snd_mono, rate=rate, N=N, sampwidth=sampwidth)

#Correr el extractor de caracteristicas
Extractor.RunExtractor()

#Guardar las caracteristicas e indicar el archivo donde se guardara (si se desea) y el indice de la caracteristica (se
guardan todas por defecto)
Extractor.SaveFeatures(outfilecsv='data/CSVFeatures.csv', outfiletxt='data/TXTFeatures.txt', cols = range(0, 19))


Se puede acceder a cualquier self.variable
por ejemplo, Extractor.elapsed_Time retornara el tiempo que demoro en calcular caracteristicas
'''


import numpy as np
import scipy as sp
import pandas as pd
import scipy.stats as st
import spectral_features as sf
import time
from scipy import signal


class FeaturesExtraction:
    def __init__(self, snd_mono, rate, N, sampwidth):
        # self.window=signal.hamming(sampwidth) #  Hamming window
        self.window = signal.boxcar(sampwidth)  # rectangular window

        # Define parameters (again)
        self.rate = rate
        self.snd_mono = snd_mono
        self.sampwidth = sampwidth
        self.N = N
        # Series Time Features
        self.f_mean = np.zeros(self.N)
        self.f_var = np.zeros(self.N)
        self.f_min = np.zeros(self.N)
        self.f_max = np.zeros(self.N)
        self.f_range = np.zeros(self.N)
        self.f_mcr = np.zeros(self.N)  # mean crossing rate
        self.f_rmq = np.zeros(self.N)   # root mean square
        self.f_skew = np.zeros(self.N)
        self.f_ntrpy = np.zeros(self.N)
        self.f_kurt = np.zeros(self.N)
        # Definir las caracteristicas que se extraeran
        self.Spectral_Flux = np.array([])
        self.Spectral_Entropy = np.array([])
        self.Spectral_Centroid = np.array([])
        self.Spread = np.array([])
        self.Spectral_Rollof = np.array([])
        self.Harmonic_ratio = np.array([])
        self.Pitch = np.array([])
        self.Spectral_mean = np.array([])
        self.Spectral_var = np.array([])

        self.elapsedTime = 0
        self.variance = 0

    def RunExtractor(self):
        InitialTime = time.time()
        for i in range(0, self.N):
            sample = self.snd_mono[i * self.sampwidth:(i + 1) * self.sampwidth]
            # 11 Series Times features
            self.f_mean[i] = sample.mean()
            self.f_var[i] = np.var(sample)
            self.f_min[i] = np.amin(sample)
            self.f_max[i] = np.amax(sample)
            self.f_range[i] = self.f_max[i] - self.f_min[i]
            zero_cross = np.where(np.diff(np.sign(sample)))
            self.f_mcr[i] = zero_cross[0].shape[0] / (self.sampwidth - 1)
            self.f_rmq[i] = np.sqrt((sample**2).mean())
            self.f_skew[i] = st.skew(sample)
            self.f_ntrpy[i] = np.sum(sp.special.entr(sample)) / self.sampwidth  # mal hecha
            self.f_kurt[i] = st.kurtosis(sample)

            X = self.snd_mono[i * self.sampwidth:(i + 1) * self.sampwidth]
            spectrum, frequency = sf.fft_window(X, self.rate, self.window)
            power = np.abs(spectrum)
            # 9 Spectral features

            SC, spread = sf.SpectralCentroidAndSpread(power, self.rate)
            Hr, pitch = sf.Harmonic(power, self.rate)

            self.Spectral_Flux = np.hstack((self.Spectral_Flux, sf.SpectralFlux(power)))
            self.Spectral_Entropy = np.hstack((self.Spectral_Entropy, sf.SpectralEntropy(power)))
            self.Spectral_Centroid = np.hstack((self.Spectral_Centroid, SC))
            self.Spread = np.hstack((self.Spread, spread))
            self.Spectral_Rollof = np.hstack((self.Spectral_Rollof, sf.SpectralRollOff(power, 0.8, self.rate)))
            self.Harmonic_ratio = np.hstack((self.Harmonic_ratio, Hr))
            self.Pitch = np.hstack((self.Pitch, pitch))
            self.Spectral_mean = np.hstack((self.Spectral_mean, power.mean()))
            self.Spectral_var = np.hstack((self.Spectral_var, np.var(power)))
        self.elapsedTime = time.time() - InitialTime
        print 'Features successfully calculated'
        print 'Elapsed time: ', self.elapsedTime, ' seconds '

    def SaveFeatures(self, outfilecsv='data/Features.csv', outfiletxt='data/Features.txt', cols=range(0, 19)):
        # Save in csv and txt file
        raw_data = {'mean': self.f_mean,  # 1
                    'variance': self.f_var,  # 2
                    'minimum': self.f_min,  # 3
                    'maximum': self.f_max,  # 4
                    'range': self.f_range,  # 5
                    'mean crossing rate': self.f_mcr,  # 6
                    'root mean square': self.f_rmq,  # 7
                    'skew': self.f_skew,  # 8
                    'average entropy': self.f_ntrpy,  # 9 var -inf
                    'kurtosis': self.f_kurt,  # 10

                    'Spectral_Flux': self.Spectral_Flux,  # 11
                    'Spectral_Entropy': self.Spectral_Entropy,  # 12
                    'Spectral_Centroid': self.Spectral_Centroid,  # 13
                    'Spread': self.Spread,  # 14
                    'Spectral_Rollof': self.Spectral_Rollof,  # 14
                    'Harmonic_ratio': self.Harmonic_ratio,  # 15
                    'Pitch': self.Pitch,  # 16 var 0
                    'Spectral_mean': self.Spectral_mean,  # 17 var 0
                    'Spectral_var': self.Spectral_var}  # 18

        df = pd.DataFrame(raw_data, columns=[
                          'mean',
                          'variance',
                          'minimum',
                          'maximum',
                          'range',
                          'mean crossing rate',
                          'root mean square',
                          'skew',
                          'average entropy',
                          'kurtosis',
                          'Spectral_Flux',
                          'Spectral_Entropy',
                          'Spectral_Centroid',
                          'Spread',
                          'Spectral_Rollof',
                          'Harmonic_ratio',
                          'Pitch',
                          'Spectral_mean',
                          'Spectral_var'
                          ])
        df.to_csv(outfilecsv)
        print 'csv Done!'
        # Caracteristica 9 no funciona 'Average Entropy'
        # La columna 0 es el indice de la observacion (header)

        # cols = [1, 2, 3, 4, 5, 6, 7, 8, 10, 11, 12, 13, 14, 15, 18] # all except 9, 16, 17
        df_time = pd.read_csv(outfilecsv, usecols=cols)
        features = df_time.values
        np.savetxt(outfiletxt, features, delimiter=',', fmt='%5.4f')
        print 'txt Done!'
        print 'Dimension of features: ', features.shape
        self.variance = np.var(features, 0)
        return features

