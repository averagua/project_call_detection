# -*- coding: utf-8 -*-
"""
Created on Tue Jun 21 10:19:35 2016

@author: 1085la
"""
import numpy as np
from scipy.fftpack import fft, fftfreq

# Calcular una ventana del spectro, las ventanas pueden ser cuadradas, hamming, etc.


def fft_window(X, sampFreq, windows):
    # Windos es la ventana que se utilizará (cuadrada, hamming, etc)
    X_windows = windows * X
    dt = 1.0 / sampFreq
    spectrum = fft(X_windows)
    # autopower=np.abs(spectrum*np.conj(spectrum))
    return spectrum, fftfreq(len(X_windows), dt)

# def SpectralCentroid(X,freq): #Spectral feature
#    magnitude_spectrum=np.sqrt((np.real(X)**2)+np.imag(X)**2)
#    power_spectrum=np.abs(magnitude_spectrum)**2
#    k=np.linspace(0,len(X),len(X))
#    SC=np.sum(power_spectrum*k)/np.sum(power_spectrum)
#   # plt.figure()
#    #plt.plot(power_spectrum)
#    return np.abs(np.sum(freq[0:-1]*X[0:-1])/np.sum(X[0:-1]))
eps = 0.00000001


def stZCR(frame):
    """Computes zero crossing rate of frame"""
    count = len(frame)
    countZ = np.sum(np.abs(np.diff(np.sign(frame)))) / 2
    return (np.float64(countZ) / np.float64(count - 1.0))


def SpectralFlux(X):
    """
    Computes the spectral flux feature of the current frame
    ARGUMENTS:
        X:        the abs(fft) of the current frame
        Xpre:        the abs(fft) of the previous frame
    """
    # compute the spectral flux as the sum of square distances:
    Xprev = X[1:len(X)]
    X = X[0: - 1]
    sumX = np.sum(X + eps)
    sumPrevX = np.sum(Xprev + eps)
    F = np.sum((X / sumX - Xprev / sumPrevX) ** 2)
    return F


def SpectralEntropy(X, numOfShortBlocks=10):
    """Computes the spectral entropy"""
    L = len(X)                         # number of frame samples
    Eol = np.sum(X ** 2)            # total spectral energy

    subWinLength = int(np.floor(L / numOfShortBlocks))   # length of sub-frame
    if L != subWinLength * numOfShortBlocks:
        X = X[0:subWinLength * numOfShortBlocks]
    # define sub-frames (using matrix reshape)
    subWindows = X.reshape(subWinLength, numOfShortBlocks, order='F').copy()
    s = np.sum(subWindows ** 2, axis=0) / (Eol + eps)  # compute spectral sub-energies
    En = -np.sum(s * np.log2(s + eps))  # compute spectral entropy
    return En


def SpectralCentroidAndSpread(X, fs):
    """Computes spectral centroid of frame (given abs(FFT))"""
    ind = (np.arange(1, len(X) + 1)) * (fs / (2.0 * len(X)))
    Xt = X.copy()
    Xt = Xt / Xt.max()
    NUM = np.sum(ind * Xt)
    DEN = np.sum(Xt) + eps

    # Centroid:
    C = (NUM / DEN)

    # Spread:
    S = np.sqrt(np.sum(((ind - C) ** 2) * Xt) / DEN)

    # Normalize:
    C = C / (fs / 2.0)
    S = S / (fs / 2.0)

    return (C, S)


def SpectralRollOff(X, c, fs):
    """Computes spectral roll-off"""
    totalEnergy = np.sum(X ** 2)
    fftLength = len(X)
    Thres = c * totalEnergy
    # Ffind the spectral rolloff as the frequency position where
    # the respective spectral energy is equal to c*totalEnergy
    CumSum = np.cumsum(X ** 2) + eps
    [a, ] = np.nonzero(CumSum > Thres)
    if len(a) > 0:
        mC = np.float64(a[0]) / (float(fftLength))
    else:
        mC = 0.0
    return (mC)


def Harmonic(frame, fs):
    """
    Computes harmonic ratio and pitch
    """
    M = np.round(0.016 * fs) - 1
    R = np.correlate(frame, frame, mode='full')

    g = R[len(frame) - 1]
    R = R[len(frame):-1]

    # estimate m0 (as the first zero crossing of R)
    [a, ] = np.nonzero(np.diff(np.sign(R)))

    if len(a) == 0:
        m0 = len(R) - 1
    else:
        m0 = a[0]
    if M > len(R):
        M = int(len(R) - 1)

    M = int(M)
    Gamma = np.zeros(M)
    CSum = np.cumsum(frame ** 2)
    Gamma[m0:M] = R[m0:M] / (np.sqrt((g * CSum[M:m0:-1])) + eps)

    ZCR = stZCR(Gamma)

    if ZCR > 0.15:
        HR = 0.0
        f0 = 0.0
    else:
        if len(Gamma) == 0:
            HR = 1.0
            blag = 0.0
            Gamma = np.zeros(M)
        else:
            HR = np.max(Gamma)
            blag = np.argmax(Gamma)

        # Get fundamental frequency:
        f0 = fs / (blag + eps)
        if f0 > 5000:
            f0 = 0.0
        if HR < 0.1:
            f0 = 0.0

    return (HR, f0)
