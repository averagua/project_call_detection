from sklearn.cluster import KMeans
import numpy as np
import matplotlib.pylab as plt


class KMeansClustering:
    def __init__(self, N_cluster, fit_features, test_features):
        self.N_cluster = N_cluster
        self.fit_features = fit_features
        self.test_features = test_features
        self.prediction = 0

    def RunClustering(self, dibujar='N'):
        kmeans = KMeans(n_clusters=self.N_cluster, init='k-means++', max_iter=1000, tol=0.0001)  # Objeto Kmeans
        kmeans.fit(self.fit_features)  # Entrenamiento de Kmeans
        kmeans.get_params(deep=True)  # Parametros del entrenamiento
        centroides = kmeans.cluster_centers_  # Obtener centroides del clustering
        self.prediction = kmeans.predict(self.test_features)  # Prediccion utilizando el objeto entrenado
        np.savetxt('data/Prediction.txt', self.prediction, fmt='%i', delimiter=',')
        k_labels = kmeans.labels_  # labels for each sample

        for i in range(min(k_labels), max(k_labels) + 1):
            print 'cluster ', i, ' size: ', np.where(k_labels == i)[0].size

        np.savetxt('data/kmeans_pred.txt', k_labels, fmt='%i')
        print 'saving done'
        if dibujar == 'Y':
            plt.figure()
            plt.plot(range(self.fit_features.shape[0]), k_labels, 'ro')
            plt.xlabel('i-th sample')
            plt.ylabel('cluster')
            plt.title('Distribucion de la muestras utilizando k-means')
            plt.grid(True)
            plt.show()
            plt.savefig('img/kmeans.png')

        print 'clustering done!'
        return self.prediction, centroides
