import numpy as np
import matplotlib.pylab as plt


class Adaptivefilters:

    def __init__(self, mu=1, order=4, eps=0.01):
        # Input arguments
        self.x = np.array([])      # Input signal
        self.d = np.array([])      # Target process

        # Parameters
        self.mu = float(mu)     # Step size
        self.order = order              # Filter length
        self.eps = eps          # Regularization factor

        # Initial values for weights (random)
        self.w = np.zeros(self.order)#np.random.normal(loc=0.0, scale=1.0, size=self.order)     # Initial weigths

        # Evolution of weights and mean square error (then calculate abs(e))
        self.w_evolution = np.array([])
        self.e_evolution = np.array([])   # MSE error

    def LMS(self, regressor_process=None, observed_process=None):
        '''

        Args:
            regressor_process: Matriz de NxM, donde N es la cantidad de muestras (tiempo) y M es la cantidad de regresores
            temporales.

            observed_process: Serie de tiempo ruidosa observada

        Returns:
            w : Ultimo peso calculado

        El algoritmo LMS encuentra el modelo de transicion de estado para un modelo lineal invariante en el tiempo
        y de ruido AWGN. Dados los instantes de tiempo para el regresor (x) y una serie de tiempo ruidosa observada (d),
        el algoritmo encuentra los pesos necesarios para el modelo lineal descrito y filtra, suaviza o predice la senal.
        '''

        self.x = regressor_process
        self.d = observed_process
        N = np.shape(self.x)[1] - self.order

        self.w_evolution = np.zeros((N, self.order))      #Initial value for the weights
        self.e_evolution = np.zeros(N)

        print 'LMS Adaptive Filter'
        R = np.dot(self.x, self.x.transpose())    # Empiric covariance
        p = np.dot(self.d, self.x.transpose())   # Cross covariance
        eig, vec = np.linalg.eig(R)
        # self.mu = 2.0/(np.amax(eig) + np.amin(eig))
        print self.mu
        for i in range(0, N):   # for each instant i
            X = self.d[i:i+self.order]    #
            self.e_evolution[i] = self.d[i+self.order-1] - np.dot(self.w, X)    # Intantanius error
            self.w_evolution[i] = self.w_evolution[i-1] + self.mu * self.e_evolution[i] * X   # Instantanius weights
            
            # self.w_evolution[i] = self.w    # weights evolution
            self.e_evolution[i] = self.e_evolution[i]**2      # error evolution
        return self.w

    def NLMS(self, regressor_process=None, observed_process=None):

        self.x = regressor_process
        self.d = observed_process
        self.w_evolution = np.copy(self.w)      #Initial value for the weights


        print 'NLMS Adaptive Filter'
        N = np.shape(self.x)[1] - self.order
        self.e_evolution = np.zeros(N)
        self.e_evolution = np.zeros(N)

        for i in range(0, N):   # for each instant i
            X = self.x[:, i]    #
            d = self.d[i]
            mu = self.mu/(self.eps + np.dot(X.transpose(), X))
            e = mu * (d - np.dot(self.w, X))
            self.w = self.w + e * X.transpose()
            self.w_evolution = np.vstack((self.w_evolution, self.w))
            self.e_evolution[i] = e**2
        print 'Last weight: '+str(self.w)
        return self.w

    def predict(self, predictor, weights):
        y = np.dot(weights, predictor)   # Estimation process
        return y


def testfctn1(w, n):
    #np.random.seed(seed=n)
    M = w.shape[0]
    d = np.random.randn(M)
    print 'Initial values for x: '+str(d)
    noise = np.random.randn(n)           # Noise
    for i in range(0, n-M):
        x = d[i:i+M]
        xn = np.dot(w, x)
        d = np.hstack((d, xn))
    d = d + np.random.normal(0, 0.05, n)
    return d
    

def testfctn2(N=1000):
    w = np.array([2.0, 0.1, -4.0, 0.5, 11])     # weigths
    M = w.shape[0]
    x = np.random.random((M, N))   # Regressor process
    v = np.random.randn(N)           # Noise, review because its posible not a gaussian noise
    d = np.dot(w, x) + v                    # Observed process
    return x, d, w