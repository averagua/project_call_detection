from scipy import signal
import numpy as np
import matplotlib.pylab as plt

def aud2image(snd, N_examples, sampwidth, rate, save_all=0):
    images = np.zeros((513, 45, N_examples))
    for i in range(0, N_examples):
        #spectrogram = plt.specgram(snd_mono[i*sampwidth:(i+1)*sampwidth], NFFT=256, Fs=6000, detrend=plt.detrend_none,
        #                           window=np.blackman(256), noverlap=128,
        #                           cmap='Greys', xextent=None, pad_to=None, sides='default',
        #                           scale_by_freq=None, mode='default')
        #print np.shape(spectrogram[0])
        #images[:, :, i] = spectrogram[0]
        #print np.shape(spectrogram[0])
        frequency, time, Spectro = signal.spectrogram(snd[i*sampwidth:(i+1)*sampwidth], fs=rate,
                                       nperseg=1024, noverlap=980, nfft=1024,
                                       window=np.hamming(1024), scaling='spectrum')
        log_spectrum = 10 * np.log10(Spectro)
        images[:, :, i] = log_spectrum
        #print 'Spectrogram', i, 'was generated'
    print 'Images dimension: ', np.shape(images)[0:2]
    print 'Number of examples: ', np.shape(images)[2]

    if save_all == 1:
        for j in range(0, 10):
            image_saved = plt.figure()
            #plt.pcolormesh(time, frequency, images[:, :, i],
            #               cmap='Greys')  # , origin="lower", aspect="auto", interpolation="none")
            plt.imshow(np.flipud(images[:, :, j]), cmap='Greys')

            plt.xlabel('Time (seg)')
            plt.ylabel('Frequency [Hz] ')
            root = 'Spectrograms/'+str(j)+'.png'
            image_saved.savefig(root)  # save the figure to file
            plt.close(image_saved)
    return frequency, time, images



