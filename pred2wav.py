
import numpy as np
from scipy.io import wavfile
import time as time
from parameters import parameters

def pred2frame(prediction, pred_index, sound, sampwidth):
    # receives an array with all the predictions of clusters, and a cluster in particular
    # returns a array with the frames of the original file which are in the given cluster
    print 'working on cluster: ', pred_index
    Call = np.array([])
    clust_index = np.where(prediction == pred_index)[0]
    print 'cluster size: ', clust_index.size
    if clust_index.size != 0:
        for i in clust_index: # each sample in the given cluster
            Call = np.hstack((Call, sound[i * sampwidth:(i + 1) * sampwidth]))
            # Call = np.hstack((Call, np.zeros(500)))  # add half second of silence
    else:
        print 'Cluster ', pred_index, 'does not contain elements'
    return np.int16(Call)


def frame2wav(Call, rate, cluster, directory=''):
    # receives the frames of a cluster in the original audio,
    # sampling rate and the cluster to evaluate
    # creates a wav file with the sound given by the cluster in the format 'call_n.wav'
    # with 'n' the number of cluster
    fname = directory+'Call' + '_' + str(cluster) + '.wav'
    wavfile.write(fname, rate, Call)
    print 'Name of file: ', fname
    print 'call :', cluster, ' is ready!'
    print 'size: ', Call.shape
    print 'duration: ', Call.shape[0] / float(rate), 'segs'
    print '-' * 60
    return

def RunConvertion(prediction, par, directory=''):
    sampFreq = par.rate
    snd_mono = par.sound
    sampwidth = par.sampwidth
    N_samp = par.N_samples
    init_time = time.time()
    count = long(0)  # count of total frames processed and saved
    # array with the clusters to evaluate
    cluster = range(int(np.amin(prediction)), int(np.amax(prediction)) + 1)
    for i in cluster:
        Call = pred2frame(prediction, i, snd_mono, sampwidth)
        count += Call.size
        frame2wav(Call, sampFreq, i, directory=directory)

    print 'Convertion time: ', time.time() - init_time
    print 'original size: ', snd_mono.shape[0]
    print 'sum of all clusters size :', count
    print 'frames lost: ', snd_mono.shape[0] - count

