'''
Do silhouette analysis to find optimal number of clusters in K-means
does DBSCAN clustering to the dataset
plots: silhouette vs number of cluslters
	 : distance to the Kth neighbour in each sample
	 : DBSCAN cluster association to each sample

'''

import numpy as np
import matplotlib.pyplot as plt
import time
from sklearn.cluster import KMeans
# Density-based spatial clustering of applications with noise
from sklearn.cluster import DBSCAN
from sklearn.metrics import silhouette_score
from sklearn.neighbors import NearestNeighbors


def kSilhouette(dataset, clust_eval=range(2, 4), dibujar='N'):
    silhouette_index = np.zeros(len(clust_eval)) # values of silhouette index
    k_time = time.time()
    for i in range(len(clust_eval)):
        n_clust = clust_eval[i]
        # K-means
        k_model = KMeans(n_clusters=n_clust, max_iter=1000).fit(dataset)  # kmeans clustering
        k_labels = k_model.labels_  # labels for each sample
        silhouette_index[i] = silhouette_score(dataset, k_labels, metric='euclidean')
    k_time = time.time() - k_time
    print 'time in k means: ', k_time, ' segs'

    if dibujar == 'Y':
        plt.figure(1)
        plt.plot(clust_eval, silhouette_index)
        plt.xlabel('number of clusters')
        plt.ylabel('silhoutte score')
        plt.title('Silhoutte analisys for K means')
        plt.grid(True)
        plt.savefig('img/silhouette.png')
        plt.show()
    return silhouette_index
    return silhouette_index


class DBSCANClustering:
    def __init__(self, dataset, minimun_points, radious, epsilon=0.1):
        self.dataset = dataset
        self.minimun_points = minimun_points
        self.epsilon = epsilon
        self.dbscan_label = 0
        self.k_neighbours = 0
        self.radious = radious

    def rundbscan(self, use_mean=False):
        dbs_time = time.time()
        # k nearest neighbors to identify epsilon
        nbrs = NearestNeighbors(n_neighbors=self.minimun_points)  # [0]distance / [1]index
        nbrs.fit(self.dataset)
        self.k_neighbours = nbrs.kneighbors()[0][:, self.minimun_points - 1]  # only the minPts-th neighbor
        print 'mean distance to k-th neighbor: ', self.k_neighbours.mean()

        if use_mean:
            self.epsilon = self.k_neighbours.mean()

        # DBSCAN clustering
        dbs = DBSCAN(eps=self.epsilon, min_samples=self.minimun_points)
        dbs.fit(self.dataset)
        self.dbscan_label = dbs.labels_  # predictec cluster of each sample

        dbs_time = time.time() - dbs_time
        print 'time in k neighbors + DBSCAN: ', dbs_time
        for i in range(min(dbs.labels_), max(dbs.labels_) + 1):
            print 'cluster ', i, ' size: ', np.where(dbs.labels_==i)[0].size
        np.savetxt('data/dbscan_pred.txt', self.dbscan_label, fmt='%i')
        return self.k_neighbours, self.dbscan_label

    # data, cluster_range, sltte_index, k_nbrs, dbs_label

    def plotdbscan(self):
        # plot k neighbors distance
        plt.figure(2)
        plt.plot(range(self.dataset.shape[0]), self.k_neighbours)
        plt.xlabel('i-th sample')
        plt.ylabel('k-th neighbor distance')
        plt.title('K nearest neighhbor')
        plt.grid(True)
        plt.savefig('img/neighbor.png')
        plt.show()

        clustM1 = np.where(self.dbscan_label[:] == -1)
        clustEL = np.where(self.dbscan_label[:] != -1)


        # plot clusters of each sample given by dbscan
        plt.figure(3)
        plt.plot(np.arange(self.dataset.shape[0])[clustM1], self.dbscan_label[clustM1], 'ro',
                 np.arange(self.dataset.shape[0])[clustEL], self.dbscan_label[clustEL], 'bo',)
        plt.xlabel('i-th sample')
        plt.ylabel('cluster')
        plt.grid(True)
        plt.savefig('img/dbscan.png')
        plt.show()

'''

data = np.loadtxt('data/Features_PCA.txt', delimiter=',')  # read csv
print 'n samples: ', data.shape[0], 'n feats: ', data.shape[1]
n_feat = data.shape[1]  # number of features

cluster_range = range(2, 21)  # number of clusters to evaluate
sltte_index = kSilhouette(data, dibujar='N')


# DBSAN parameter heuristic:
# minPts >= D+1 with D dimension of samples
# epsion, using k-distance graph with k=minPts

minPts = n_feat + 1
# radious = input('enter epsilon: ')
radious = 0.1


DBS = DBSCANClustering(dataset=data, minimun_points=minPts, radious=radious)
k_nbrs, dbs_label = DBS.rundbscan()
DBS.savedbscan()
# DBS.plotdbscan()
'''