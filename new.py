from parameters import parameters
from Other import PlotSpectrogram
import matplotlib.pylab as plt
import matplotlib.mlab as mlab
from scipy import signal
import numpy as np
from matplotlib import pyplot as plt
import scipy.io.wavfile as wav
from numpy.lib import stride_tricks


""" short time fourier transform of audio signal """

DBCall = 'Call_0D.wav'
call1 = 'snd_detection/llamados/call_2.wav'
bm048a004 = 'bm048a001.wav'
compilation = 'compilation.wav'

llamado = parameters(wavfile=call1, tsamp=1)
llamado.load_all(sndtype='stereo')
title2 = 'Espectrograma de un llamado'
PlotSpectrogram(llamado.sound, llamado.rate, NFFT=256, nov=128, title=title2)
plt.figure()
plt.plot(np.linspace(0, len(llamado.sound)/llamado.rate, len(llamado.sound)), llamado.sound)
plt.title('Serie de tiempo de un llamado')
plt.xlabel('tiempo [ms]')
plt.ylabel('Amplitud')
plt.grid(True)
plt.show()


call_1 = parameters(wavfile=compilation, tsamp=1)
call_1.load_all(sndtype='stereo')

rate = call_1.rate  # sampling frequency
snd_mono = call_1.sound  # data in np array
N = call_1.N_samples
sampwidth = call_1.sampwidth

# initialt = 3*60 + 13.5
# dt = 5.5 # seconds
# title = 'Espectrograma de un splash'

# Sonar
title = 'Espectrograma de un sonar y lobo marino'
initialt = 7*60 + 58
dt = 6
initial = call_1.rate*initialt
final = call_1.rate*(initialt+dt)
PlotSpectrogram(call_1.sound[initial:final], call_1.rate, NFFT=256, nov=128, title=title)
num = 256
plt.figure()
plt.plot(np.linspace(initialt, dt, len(call_1.sound[initial:final])), call_1.sound[initial:final])
plt.title('Serie de tiempo de un sonar y lobo marino')
plt.xlabel('tiempo [ms]')
plt.ylabel('Amplitud')
plt.grid(True)

plt.show()
for i in range(10, 110, 10):
    pass
    # frequency, time, Spectro = signal.spectrogram(call_1.sound[call_1.rate*i:call_1.rate*(i+2)],
    #                                               fs=call_1.rate, nfft=num,
    #                                              detrend='linear', scaling='spectrum', window=np.hamming(num))
    # Pxx, freqs, bins, im = PlotSpectrogram(call_1.sound[call_1.rate*i:call_1.rate*(i+2)], call_1.rate, NFFT=256, nov=128)

    # plt.figure()
    # plt.pcolormesh(time, frequency, Spectro, cmap=plt.cm.gray)
    # plt.show()
