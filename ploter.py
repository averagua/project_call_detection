import numpy as np
import matplotlib.pylab as plt
pred = np.loadtxt('data/kmeans_pred.txt', delimiter=',')

index0 = np.where(pred == 0)
index1 = np.where(pred == 1)
index2 = np.where(pred == 2)
index3 = np.where(pred == 3)
# llamados = pred[pred == 2]
# print llamados.shape
# print pred.shape
samples = np.linspace(0, len(pred), len(pred), dtype=int)
plt.figure()
plt.scatter(samples[index0], pred[index0], c='g', label='Ruido')
plt.scatter(samples[index1], pred[index1], c='y', label='Llamado y peces')
plt.scatter(samples[index2], pred[index2], c='r', label='Ruido de agua')
plt.scatter(samples[index3], pred[index3], c='b', label='Lobos marinos')
plt.xlabel('i-th muesdtra')
plt.ylabel('Grupo')


plt.legend(bbox_to_anchor=(1.05, 1), loc=5, ncol=2)
plt.show()
