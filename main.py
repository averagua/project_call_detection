from parameters import parameters
from Extract_features import FeaturesExtraction
from preprocessingFeatures import PreprocessingFeatures
import numpy as np
import matplotlib.pylab as plt
from scipy.io import wavfile
from KMClustering import KMeansClustering
import pred2wav
from Other import PlotSpectrogram
from dbscan_clustering import kSilhouette
from dbscan_clustering import DBSCANClustering
import sys


# Lineas de codigo que cargan dos cantos conocidos y el archivo de ejemplo
# No es necesario utilizar ventanas de tiempo tan finas
# Se probaron ventanas de 0.5, 0.7, 0.9 y 1. Siendo esta ultima la mejor


compilation = 'compilation.wav'
DBCall = 'Call_0D.wav'
KMCall = 'Call_0K.wav'
bm048a001 = 'bm048a001.wav'     # rate = 12000
bm048a002 = 'bm048a002.wav'     # rate = 12000
bm048a004 = 'bm048a004.wav'
bm048a005 = 'bm048a005.wav'

params = parameters(wavfile=compilation, tsamp=1)
params.load_all(sndtype='stereo')

rate = params.rate  # sampling frequency
snd_mono = params.sound  # data in np array
N = params.N_samples
sampwidth = params.sampwidth


'''
cols = [1, 2, 3, 4, 5, 6, 8, 10, 11, 12, 13, 14, 15, 16, 17, 18] # Para Call 0
params = parameters(wavfile='Call_0.wav', tsamp=0.9)
params.load_all(sndtype='mono')
rate = params.rate  # sampling frequency
snd_mono2 = params.sound  # data in np array
N = params.N_samples
sampwidth = params.sampwidth
'''
# cols = [11, 12, 13, 14, 15, 16, 17, 18]
cols = [1, 2, 3, 4, 5, 6, 8, 10, 11, 12, 13, 14, 15, 16, 17, 18]
prepro = True
if prepro == True:
    ExtractorMachine = FeaturesExtraction(snd_mono=snd_mono, rate=rate, N=N, sampwidth=sampwidth)
    ExtractorMachine.RunExtractor()
    print 'Numero de ejemplos:', N
    features = ExtractorMachine.SaveFeatures()
    print 'Dimension de caracteristicas funcionales: ', features[:, cols].shape
    preprocesamiento = PreprocessingFeatures(features[:, cols])
    features_scale = preprocesamiento.scale()
    pca_data = preprocesamiento.PCAnalisys(datatouse='scale', n_components=len(cols), plot='Y')
    print 'Dimension de caracteristicas luego de pca: ', pca_data.shape

features = np.loadtxt('data/Features_PCA.txt', delimiter=',')
print 'Dimension de caracteristicas cargadas:', features.shape
# cluster_type = raw_input('Ingrese el cluster que desea ejecutar (K o D): ')

def FirstLayer(data, image=False, wavconvertion =False):
    # Agrupamiento de la primera capa
    minPts = data.shape[1] + 1
    # radious = input('enter epsilon: ')
    radious = 0.8
    DBS = DBSCANClustering(dataset=data, minimun_points=minPts, radious=radious)
    k_nbrs, dbs_label = DBS.rundbscan(use_mean=True)
    print 'DBSCAN was executed'
    if wavconvertion == True:
        pred2wav.RunConvertion(dbs_label, par=params, directory='snd_detection_dbscan/')
        print 'Done convertion!'
    if image == True:
        plt.figure()
        plt.scatter(data[np.where(dbs_label == -1), 0], data[np.where(dbs_label == -1), 1], c='b')
        plt.scatter(data[np.where(dbs_label == 0), 0], data[np.where(dbs_label == 0), 1], c='y')

        plt.title('Agrupamiento de splash')
        plt.xlabel('Primera componente')
        plt.ylabel('Segunda componente')
        plt.show()
        DBS.plotdbscan()
    return dbs_label

# Correr el agrupamiento de la primera capa
# dbs_label = np.loadtxt('data/dbscan_pred.txt', delimiter=',')

# En el cluster 0 quedaran todos los sonidos no splash
# Elegir todos los datos de este grupo

def SecondLayer(DBfile='snd_detection_dbscan/Call_0.wav', N_clusters = 2, Silueta=False, prepro=True):
    params2 = parameters(wavfile=DBfile, tsamp=1)
    params2.load_all(sndtype='stereo')
    cols = [1, 2, 3, 4, 5, 6, 8, 10, 11, 12, 13, 14, 15, 16, 17, 18]

    if prepro == True:
        ExtractorMachine2 = FeaturesExtraction(snd_mono=params2.sound, rate=params2.rate,
                                               N=params2.N_samples, sampwidth=params2.sampwidth)
        ExtractorMachine2.RunExtractor()
        features2 = ExtractorMachine2.SaveFeatures(outfiletxt='data/Features2.txt')
        print 'Numero de ejemplos:', N
        # features = ExtractorMachine2.SaveFeatures()
        preprocesamiento2 = PreprocessingFeatures(features2[:, cols])
        preprocesamiento2.scale()
        preprocesamiento2.minmax()
        # data = preprocesamiento2.PCAnalisys(datatouse='minmax', n_components=len(cols), plot='Y')
        data = preprocesamiento2.PCAnalisys(datatouse='scale', n_components=len(cols), plot='Y')

        print 'Dimension de caracteristicas segunda capa: ', data.shape

    if Silueta == True:
        cluster_range = range(2, 21)  # number of clusters to evaluate
        sltte_index = kSilhouette(data, clust_eval=cluster_range, dibujar='Y')

    KM = KMeansClustering(N_cluster=N_clusters, fit_features=data, test_features=data)
    pred, centroid = KM.RunClustering(dibujar='Y')
    print 'K-Means was executed'
    # pred = np.loadtxt('data/kmeans_pred.txt', delimiter=',')

    plt.figure()
    plt.title('Agrupamiento utilizando KMeans')
    plt.scatter(data[:, 0], data[:, 1], c=pred)
    plt.xlabel('Primera componente')
    plt.xlabel('Segunda componente')

    plt.figure()
    plt.title('Agrupamiento utilizando KMeans')
    plt.scatter(data[:, 0], data[:, 2], c=pred)
    plt.xlabel('Primera componente')
    plt.xlabel('Segunda componente')

    plt.figure()
    plt.scatter(data[:, 1], data[:, 2], c=pred)
    plt.xlabel('Primera componente')
    plt.xlabel('Segunda componente')
    plt.show()

    pred2wav.RunConvertion(pred, par=params2, directory='snd_detection_kmeans/')
    print 'Done convertion!'
    return data, pred


dbs_label = FirstLayer(features, image=True, wavconvertion=True)
km_data, km_label = SecondLayer(N_clusters=4, Silueta=True)

