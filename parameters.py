'''
Para ejecutar:
from parameters(file) import parameters(class)
params = parameters(wavfile ='filename.wav', tsamp=0.5)
params.load_all(sndtype = 'stereo') (or mono)
params.rate
params.sound
'''
import scipy.io.wavfile as wav
import numpy as np
from scipy import signal


class parameters:
    def __init__(self, wavfile='Example_bm048a003.wav', tsamp=0.5):
        self.filename = wavfile  # file name
        self.rate = 0  # sample rate of the audio
        self.sound = 0  # matrix with the data
        self.t_samp = tsamp  # time of each sample
        self.sampwidth = 0  # sample width in frames
        self.N_samples = 0  # number of samples in the data

    def load_all(self, sndtype='mono'):
        self.rate, self.sound = wav.read(self.filename)  # read as numpy array
        try:
            self.sound = self.sound.mean(axis=1)  # convert to mono
        except IndexError:
            print 'Archivo de entrada formato mono, se mantiene'
        self.sampwidth = int(self.t_samp * self.rate)  # width in frames
        self.N_samples = int(np.trunc(self.sound.shape[0] / self.sampwidth))  # number of sample

        print 'Filename:            ', self.filename
        print 'Sound type:          ', sndtype
        print 'Sound dimension:     ',  self.sound.shape
        print 'Times windows (s):   ', self.t_samp
        print 'Rate:                ', self.rate
        print 'Sampwidth:           ', self.sampwidth
        print 'Number of samples:   ', self.N_samples

    def filter(self, frec):
        order = 500
        b, a = signal.iifilter(order,[0, 1000*2*np.pi], btype='lowpass' ,ftype='cheby2' , analog=True)
        self.sound = signal.filtfilt(b, a, self.sound)
        print 'Filtrado!'
        return
