

'''
Ejemplo de uso:

Cargar y elegir las caracteristicas que se utilizaran
features = np.loadtxt('data/Features.txt', delimiter=',')
cols = [1, 2, 3, 4, 5, 7, 8]
#cols = [11, 12, 13, 14, 15, 16 , 17, 18]
print 'Dimension de caracteristicas: ', features[:, cols].shape

# Iniciar el objeto de preprocesamiento
preprocesamiento = PreprocessingFeatures(features[:, cols])
# Ejecutar los metodos para preprocesar
features_scale = preprocesamiento.scale()
preprocesamiento.minmax()
preprocesamiento.PCAnalisys()

# Si bien los metodos retornan los datos, se pueden acceder a ellos mediante los constructores (self)
preprocesamiento.features_pca

plt.figure()
plt.scatter(preprocesamiento.features_pca[:, 0], preprocesamiento.features_pca[:, 1])
plt.show()

'''
import numpy as np
import matplotlib.pyplot as plt
from sklearn import preprocessing
from sklearn.decomposition import PCA


class PreprocessingFeatures:
    def __init__(self, features):
        self.features = features
        self.features_minmax = 0
        self.features_scale = 0
        self.features_pca = 0
        self.pca_covariance = 0

    def scale(self, filename='data/Features_Scale.txt'):
        self.features_scale = preprocessing.scale(self.features)
        np.savetxt(filename, self.features_scale, fmt='%2.5f', delimiter=',')
        print 'The scaled data were stored in ', filename
        return self.features_scale

    def minmax(self, filename='data/Features_MinMax.txt'):
        min_max = preprocessing.MinMaxScaler()
        self.features_minmax = min_max.fit_transform(self.features)
        np.savetxt(filename, self.features_minmax, fmt='%2.5f', delimiter=',')
        print 'The minmax data were stored in ', filename

        return self.features_minmax

    def PCAnalisys(self, datatouse='scale', n_components=2, filename='data/Features_PCA.txt', plot='N'):
        if datatouse == 'scale':
            data = self.features_scale
            title = 'Aporte de la varianza de cada caracteristica usando PCA'
            print 'Scale data will be use'
        elif datatouse == 'minmax':
            data = self.features_minmax
            title = 'Principal Component variance using minmax data'
            print 'Min max data will be use'
        pca = PCA(n_components=n_components)
        pca.fit(data)
        self.pca_covariance = pca.get_covariance()
        self.features_pca = pca.transform(data)
        np.savetxt(filename, self.features_pca, fmt='%2.5f', delimiter=',')

        if plot == 'Y':
            n_feat = self.features_pca.shape[1]  # number of features
            plt.figure()
            plt.plot(range(1, n_components+1), pca.explained_variance_ratio_)  # X vs Y
            print pca.explained_variance_ratio_
            plt.xlabel('Components')
            plt.ylabel('Variance ratio')
            plt.title(title)
            plt.grid(True)
            plt.savefig('img/PCA.png')

            # plt.figure(2)
            # plt.scatter(self.features_pca[:, 0], self.features_pca[:, 1])
            # plt.figure(3)
            # plt.scatter(self.features_pca[:, 0], self.features_pca[:, 2])
            # plt.figure(4)
            # plt.scatter(self.features_pca[:, 1], self.features_pca[:, 2])
            plt.show()
        print 'The PCA data were stored in ', filename
        return self.features_pca

