from parameters import parameters
import matplotlib.pylab as plt
from scipy.io import wavfile
import numpy as np
import sys

class compilatefiles:
    def __init__(self, filename1='', filename2='', tsamp=1):
        self.rate = 0  # sample rate of the audio
        self.longound = 0  # matrix with the data
        self.t_samp = tsamp  # time of each sample
        self.sampwidth = 0  # sample width in frames
        self.N_samples = 0  # number of samples in the data

        try:
            call_1 = parameters(wavfile=filename1, tsamp=tsamp)
            call_1.load_all(sndtype='mono')

            call_2 = parameters(wavfile=filename2, tsamp=tsamp)
            call_2.load_all(sndtype='mono')
        except IOError:
            print 'File doesnt exist'
            sys.exit(0)
        if call_1.rate != call_2.rate:
            print filename1, 'and', filename2, 'not have a same rate'
            sys.exit(0)
        else:
            self.rate = call_2.rate
        self.sound1 = call_1.sound
        self.sound2 = call_2.sound

    def compilate(self, outfile='longcompilation.wav'):
        self.longsound = np.hstack((self.sound1, self.sound2))
        wavfile.write(outfile, self.rate, np.int16(self.longsound))
        print 'Se han compilado exitosamente'

def PlotSpectrogram(data, rate, NFFT=256, nov=64, title='Espectrograma'):
    plt.figure(figsize=(10, 5))
    Pxx, freqs, bins, im = plt.specgram(data,
                                        NFFT=NFFT,
                                        Fs=rate,
                                        noverlap=nov,
                                        cmap=plt.cm.Accent,
                                        window=np.hamming(NFFT),
                                        mode='magnitude')
    plt.xlabel('Time [s]')
    plt.ylabel('Frequency [Hz] ')
    # plt.xlim([0, 6])
    plt.ylim([0, 1300])
    plt.title(title)

    # plt.colorbar()
    return Pxx, freqs, bins, im
def generatecompilation():

    example = parameters(wavfile='compilation.wav', tsamp=1)
    example.load_all(sndtype='mono')

    call_1 = parameters(wavfile='bm054a001', tsamp=1)
    call_1.load_all(sndtype='stereo')

    call_2 = parameters(wavfile='snd_detection/llamados/call_2.wav', tsamp=1)
    call_2.load_all(sndtype='stereo')

    PlotSpectrogram(data=call_1.sound, rate=call_1.rate, nov=110)
    PlotSpectrogram(data=call_2.sound, rate=call_1.rate, nov=180)

    N = example.N_samples + call_1.N_samples + call_2.N_samples

    call = np.hstack((call_2.sound, call_1.sound))
    # PlotSpectrogram(data=call, rate=call_1.rate, nov=110)
    print 'Dimension luego de concatenar llamados: ', call.shape
    labels = np.hstack((np.zeros(call_1.N_samples + call_2.N_samples), np.ones(N)))
    snd_mono = np.hstack((call, example.sound))
    wavfile.write('compilation.wav', 6000, np.int16(snd_mono))
    return labels, N